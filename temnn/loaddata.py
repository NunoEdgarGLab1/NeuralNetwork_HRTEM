"""Helper functions for loading training data and neural nets."""
import tensorflow.keras as keras
from temnn.data.dataset import DataEntry,DataSet
from glob import glob
import os
import numpy as np

def load_training(data_dirs):
    "Load data folder (or list of data folders)."
    if isinstance(data_dirs, str):
        data_dirs = [data_dirs]
    if not data_dirs:
        raise ValueError("Empty list of folders given to load_training.")
    entries = []
    for data_dir in data_dirs:
        waves=sorted(glob(os.path.join(data_dir, "wave", "wave_*.npz")))
        points=sorted(glob(os.path.join(data_dir, "points", "points_*.npz")))
        print("Reading {} configurations from {}.".format(len(waves), data_dir))
        if not len(waves):
            raise ValueError("The folder {} contains no configurations!".format(data_dir))
        entries += [DataEntry(wave=w, points=p) for w,p in zip(waves,points)]
    if not len(entries):
        raise ValueError("The dataset appears to be empty!")
    return DataSet(entries)

# For script compatibility
load = load_training

def find_training_folders(folder):
    """Examines a folder with training data.

    If the folder contains a 'wave' subfolder, it is a data folder and is returned.

    If not, all subfolders are tested (recursively) and any folders with
    'wave' subfolders are returned.
    """
    if os.path.isdir(os.path.join(folder, "wave")):
        return [folder]
    else:
        folders = []
        for item in os.listdir(folder):
            item = os.path.join(folder, item)
            if os.path.isdir(item) and not item.endswith('-test'):
                folders += find_training_folders(item)
        return folders

def load_CNN(graph_path, graph_func, size, num_gpus=1, image_features=1, num_classes=1):
    "Load the Keras neural net, and return a Model."
    if num_gpus == 1:
        x = keras.Input(shape=tuple(size)+(image_features,))
        model = graph_func(x, output_features=num_classes)
        model.load_weights(graph_path)
    else:
        with tf.device('/cpu:0'):
            x = keras.Input(shape=tuple(size)+(image_features,))
            model = net.graph(x, output_features=num_classes)
            model.load_weights(graph_path)
        model = multi_gpu_model(model, gpus=num_gpus)    
    return (x, model)
