See `HOW TO analyze nanoparticle image sequences
<../HOWTO/HOWTO_nanop|Generates labels for dataset particle_sequences.rst>`_.   
  
Files
====

random_cluster.py
   Creates a random cluster of atoms.
labels.py
   Generates labels for dataset.
make_cluster_110.py
   Generates the exit waves and spatial points for
   Gold nanoclusters with a 110 orientation.
   Will generate a folder to store the output.
make_cluster_110.bsub
   Sends Python script to the HPC CPU.
