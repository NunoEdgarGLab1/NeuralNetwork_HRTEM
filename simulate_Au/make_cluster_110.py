## for importing modules from parent directories
import sys
sys.path.insert(0, '../../')
sys.path.insert(0, '../../NeuralNetwork_HRTEM')

## import modules
import numpy as np
from pyqstem.util import atoms_plot
from temnn.imagesimul.labels import project_positions,create_label
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import fcluster, linkage
from pyqstem import PyQSTEM
from pyqstem.imaging import CTF
from random_cluster import RandomCluster
from util import discrete_cmap
from ase.io import write
import os
import time
import shutil
import argparse
from multiprocessing import Pool

def driver2(args):
    driver(**args)
    
def driver(first_number, last_number, dir_name, N, sampling, latconst, gridsize):
    # Parameters
    L=sampling*N
    num_classes=1

    # Create the main object for making clusters
    rc=RandomCluster(latconst, gridsize)   # Parameters are lattice constant and grid size
    qstem=PyQSTEM('TEM')

    # Create the models
    for i in range(first_number, last_number):
        radius=5+np.random.rand()*16

        lengths100=np.random.uniform(radius,radius+.2*radius,6)
        lengths111=np.random.uniform(radius-.2*radius,radius,8)

        rc.create_seed(lengths100,lengths111)
        rc.build(int(np.sum(rc.active)/4.),10,2)
        rc.build(int(np.sum(rc.active)/4.),2,2)

        atoms=rc.get_cluster('Au')

        atoms.rotate(v='y',a=45.0)

        # Find the atomic columns
        positions=atoms.get_positions()
        clusters = fcluster(linkage(positions[:,:2]), 1, criterion='distance')
        unique,indices=np.unique(clusters, return_index=True)

        atoms.rotate(v='z',a=np.random.random()*360)

        omega=np.random.random()*360
        alpha=np.random.random()*2.5

        atoms.rotate(v='z',a=omega,center='COP')
        atoms.rotate(v='y',a=alpha,center='COP')
        atoms.rotate(v='z',a=-omega,center='COP')

        atoms.center(vacuum=0)
        size=np.diag(atoms.get_cell())
        if size.max() > L:
            raise RuntimeError("System too big, should have size {} but has {}.".format(L, str(size)))
        atoms.set_cell((L,)*3)
        atoms.center()

        tx=(L-size[0]-5)*(np.random.rand()-.5)
        ty=(L-size[1]-5)*(np.random.rand()-.5)

        atoms.translate((tx,ty,0))

        # Find the positions of the columns and the number of atoms per column.
        positions=atoms.get_positions()[:,:2]
        sites = np.array([np.mean(positions[clusters==u],axis=0) for u in unique])
        heights = np.array([np.sum(clusters==u) for u in unique])

        #positions,counts=project_positions(atoms,distance=.8,return_counts=True)

        model = atoms

        wave_size=(int(model.get_cell()[0,0]*10),int(model.get_cell()[1,1]*10))
        qstem.set_atoms(model)
        qstem.build_wave('plane',300,wave_size)
        qstem.build_potential(int(model.get_cell()[2,2]*2))
        qstem.run()
        wave=qstem.get_wave()

        wave.array=wave.array.astype(np.complex64)

        positions,counts=project_positions(model,distance=.8,return_counts=True)
        label=create_label(positions/sampling,(N,)*2,width=12,num_classes=num_classes,null_class=False)

        np.savez('{0}/points/points_{1:04d}.npz'.format(dir_name,i), sites=sites, heights=heights)
        wave.save('{0}/wave/wave_{1:04d}.npz'.format(dir_name,i))
        write('{0}/model/model_{1:04d}.cfg'.format(dir_name,i),model)

        print('iteration', i, flush=True)
    
N=1200
sampling = 0.05
latconst = 4.08
gridsize = 22

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("folder", help="The name of the folder (in ../data) where the output is placed.")
    parser.add_argument("number", type=int,
                        help="The desired number of training/testing examples (incl. any that are already done).")
    parser.add_argument("-s", "--start", type=int, default=0,
                        help="Starting value (if some training data was already made).")
    parser.add_argument("-n", "--numproc", type=int, default=1,
                        help="Number of processes to use (CPU cores).  Use -1 for all available cores.")
    parser.add_argument('--train', dest='test', action='store_false',
                       help="Training data? (default)")
    parser.add_argument('--test', dest='test', action='store_true',
                       help="Test data?")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    if args.numproc > args.number - args.start:
        raise ValueError("Cannot run on more CPU cores than the number of training data.")

    if args.numproc < -1 or args.numproc == 0:
        raise ValueError("Number of CPU cores must be a positive integer, or -1 for all cores.")

    numproc = args.numproc
    if numproc == -1:
        if 'LSB_MAX_NUM_PROCESSORS' in os.environ:
            numproc = int(os.environ['LSB_MAX_NUM_PROCESSORS'])
        else:
            numproc = os.cpu_count()

    dir_name = os.path.join('../', 'simulation_data', args.folder)

    print("Generating samples {} to {} in folder {} using {} process(es)".format(
        args.start, args.number, dir_name, numproc))

    # Make sure working folders exist
    # Generate test set?
    if args.test==True:
        dir_name += '-test'
    if not os.path.exists(dir_name):
        print("Creating folder", dir_name)
        for subf in ['wave', 'model', 'points']:
            os.makedirs(os.path.join(dir_name, subf))

    # Keep a copy of this script for reference
    shutil.copy2(__file__, dir_name)

    before = time.time()

    if numproc == 1:
        # Running on a single core.
        driver(first_number=args.start, last_number=args.number, dir_name=dir_name, N=N,
               sampling=sampling, latconst=latconst, gridsize=gridsize)
    else:
        data = []
        ndata = args.number - args.start
        for i in range(numproc):
            data.append(dict(
                first_number=args.start + i * ndata // numproc,
                last_number = args.start + (i+1) * ndata // numproc,
                dir_name=dir_name,
                N=N,
                sampling=sampling,
                latconst=latconst,
                gridsize=gridsize,
                ))
        with Pool(numproc) as pool:
            pool.map(driver2, data)

    print("Time to simulate models: {:.2f} s.".format(time.time() - before))
