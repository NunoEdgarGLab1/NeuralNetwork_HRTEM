Files
====

ktrain_onthefly.py
  Training script for a single class, where images are generated
  on the fly.
  Will generate a folder 'NNname_onthefly_training_data' to
  store the output. (NNname is the given neural network
  name.)
ktrain-multi_onthefly.py
  Training script for multiple classes, where images are
  generated on the fly.
  Will generate a folder 'NNname-multi_onthefly_training_data' 
  to store the output. (NNname is the given neural network
  name.)
ktrain-multi_precomputed.py
  Training script for multiple classes, loading precomputed images
  into the network.
  Will generate a folder 'NNname-multi_precomputed_training_data' 
  to store the output. (NNname is the given neural network
  name.)
ktrain.bsub
  Sends Python script to the HPC GPU.
ktrain.dtucompute.sh
  Sends Python script to dtu compute GPU.
