import sys
sys.path.insert(0, '../')

import matplotlib
matplotlib.use('AGG')
import os
from datetime import datetime
from glob import glob
import numpy as np
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.utils import multi_gpu_model
#from temnn.knet import net
from temnn.data.dataset import DataSet, DataEntry
from temnn.imagesimul.makeimages import MakeImages
#import sys
import os
import time
import platform
import shutil
import json
import argparse
import contextlib

num_epochs = 50 # number of training epochs
save_epochs = 1
#loss_type = 'binary_crossentropy' # mse or binary_cross_entropy
#loss_type = 'mse' # mse or binary_cross_entropy
optimizer = 'rmsprop'
metrics=['accuracy',
        tf.keras.metrics.Precision(),
        tf.keras.metrics.Recall()]

def summary_image(y,size):
    return tf.reshape(tf.cast(tf.argmax(input=y,axis=3),tf.float32),(1,)+size+(1,))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("NNname", help="The name of the neural network being used.")
    parser.add_argument("datafolder", help="The path and name of the folder where the data is placed (remember to finish with /).")
    parser.add_argument("folder", help="The name of the folder (in ../(NNname)_trained_data/)where the output is placed.")
    args = parser.parse_args()
    
    # Print on which host this is running (useful for troubleshooting on clusters).
    print("{}: Running on host '{}'".format(
        datetime.now().strftime("%Y%m%d-%H%M%S"),
        platform.node()
    ))

    rng = np.random.default_rng()

    # A number of output files and folders contain a timestamp as part of their name.
    foldername = args.NNname + '_precomputed_trained_data'
    folderlabel = os.path.join('../', foldername, args.folder)
   
    if not os.path.exists(folderlabel):
        print("Creating folder", folderlabel)
        os.makedirs(folderlabel)

    timestamp = datetime.now().strftime("%Y%m%d-%H%M%S")
    summary_dir = "summaries/" + timestamp + "/"
    graph_path = folderlabel + '/model-{}'
    debug_dir = "debug/" +  timestamp

    # Load metadata
    with open(os.path.join(args.datafolder, 'parameters.json')) as json_file:
        parameters = json.load(json_file)
    imagedir = os.path.join(args.datafolder, 'images_labels')

    # Load metadata about the validation set
    if args.datafolder[-1] == '/':
        validationdatafolder = args.datafolder[:-1] + '-test'
    else:
        validationdatafolder = args.datafolder + '-test'
    with open(os.path.join(validationdatafolder, 'parameters.json')) as json_file:
        validation_parameters = json.load(json_file)
    validation_imagedir = os.path.join(args.datafolder, 'images_labels')
        
    # Make folders for output
    graph_dir = os.path.dirname(graph_path)
    if graph_dir and not os.path.exists(graph_dir):
        os.makedirs(graph_dir)
    if parameters['debug']:
        os.makedirs(os.path.join(folderlabel,debug_dir))
    logfile = open(os.path.join(graph_dir, timestamp + '.log'), "wt", buffering=1)

    # Read number of training images
    images_per_epoch = parameters['images_per_epoch']
    image_epochs = parameters['image_epochs']
    parameters['train_epochs'] = num_epochs

    # Read number of validation images
    validation_images_per_epoch = validation_parameters['images_per_epoch']
    validation_image_epochs = validation_parameters['image_epochs']
    
    # Keep a copy of this script for reference
    shutil.copy2(__file__, graph_dir)
    with open(os.path.join(graph_dir, 'parameters.json'), "wt") as paramfile:
        json.dump(parameters, paramfile, sort_keys=True, indent=4)
    
    # Determine number of GPUS
    cudavar = 'CUDA_VISIBLE_DEVICES'
    if cudavar in os.environ:
        cudadevices = os.environ[cudavar]
        numgpus = len(cudadevices.split(','))
        print(cudavar, '=', cudadevices)
        print("Found {} GPU devices".format(numgpus))
    else:
        numgpus = 1
        
    # The model is pretty big, we probably can only train on one data
    # point at a time without running out of GPU memory.
    batch_size = numgpus * 2

    image_size = tuple(parameters['image_size']) # spatial dimensions of input/output
    if parameters.get('multifocus', None):
        image_features = parameters['multifocus'][0]
    else:
        image_features = 1 # depth of input data
    num_classes = parameters['num_classes'] # number of predicted class labels
    if num_classes > 1:
        loss_type = 'categorical_crossentropy'
    else:
        loss_type = 'binary_crossentropy'

    num_in_epoch = images_per_epoch//batch_size
    num_iterations=num_epochs*num_in_epoch
    
    outputcounter = 0

    assert(batch_size % numgpus == 0)

    # Create the neural network
    # Import the specified neural network
    print('Importing', args.NNname)
    if args.NNname == 'Unet':
        from temnn.knet import Unet
        net = Unet
        batch_size = numgpus * 2
        print('Training Unet architecture')
    elif args.NNname == 'MSDnet':
        from temnn.knet import MSDnet
        net = MSDnet
        batch_size = numgpus
        print('Training MSDnet architecture')
    else:
        print('Please specify a correct Neural Network (Unet, or MSDnet)')

    if numgpus > 1:
        strategy = tf.distribute.MirroredStrategy()
        strategy_scope = strategy.scope
        print("*** Replicas:", strategy.num_replicas_in_sync)
        print(strategy)
        assert strategy.num_replicas_in_sync == numgpus
    else:
        strategy_scope = contextlib.nullcontext
    with strategy_scope():
        x = keras.Input(shape=image_size+(image_features,))
        model = net.graph(x, output_features=num_classes,
                          reflection_padding=True)
        model.compile(optimizer=optimizer, loss=loss_type,
                      metrics=metrics)

    print("Model summary:", args.NNname)
    model.summary()

    if not os.path.exists(os.path.join(folderlabel,summary_dir)):
        os.makedirs(os.path.join(folderlabel,summary_dir))
    
    print("Starting timing")
    before = time.time()
    print("Loading validation set", flush=True)
    validation_images = np.empty((validation_images_per_epoch,
                                      image_size[0], image_size[1],
                                      image_features),
                                     np.float32)
    validation_labels = np.empty((validation_images_per_epoch,
                                      image_size[0], image_size[1],
                                      num_classes),
                                     np.float32)
    for i in range(validation_images_per_epoch):
        data = np.load(os.path.join(validation_imagedir,
                                        'image_label_{:03d}_{:04d}.npz'.format(0, i)))
        validation_images[i] = data['image']
        validation_labels[i] = data['label']
        if (i + 1) % 500 == 0:
            print("    loaded {} images.".format(i+1), flush=True)

    # Write headers to the logfile:
    line = ''
    #for m in ['loss',] + metrics:
    for m in ['loss', 'accuracy', 'precision', 'recall']:
        mm = 'val_' + m
        line = line + '{:<15s} {:<15s} '.format(m, mm)
    print(line.strip(), file=logfile, flush=True)
    
    # Optimize the model, saving every save_epoch epoch.
    images = np.empty((images_per_epoch, image_size[0], image_size[1], image_features),
                          np.float32)
    labels = np.empty((images_per_epoch, image_size[0], image_size[1], num_classes),
                          np.float32)
    for epoch in range(num_epochs):
        actual_epoch = epoch % image_epochs
        print("Loading training epoch {} [{}] / {}.".format(epoch, actual_epoch, num_epochs), flush=True)
        for i in range(images_per_epoch):
            data = np.load(os.path.join(imagedir,
                                            'image_label_{:03d}_{:04d}.npz'.format(actual_epoch, i)))
            images[i] = data['image']
            labels[i] = data['label']
            if (i + 1) % 500 == 0:
                print("    loaded {} images.".format(i+1), flush=True)
        # Random permutation
        permut = rng.permutation(images_per_epoch)
        images = images[permut]
        labels = labels[permut]

        history = model.fit(images, labels, batch_size=batch_size, epochs=1,
                                validation_data=(validation_images, validation_labels))
        history = history.history  # Extract the actual history dictionary
        line = ''
        #for m in ['loss',] + metrics:
        for m in ['loss', 'accuracy', 'precision', 'recall']:
            mm = 'val_' + m
            print("{} = {}    {} = {}".format(m, history[m][0], mm, history[mm][0]), flush=True)
            line = line + '{:<15.8f} {:<15.8f} '.format(history[m][0], history[mm][0])
        print(line.strip(), file=logfile, flush=True)
    
        # Save 
        if (epoch+1) % save_epochs == 0:
            #model.save_weights(graph_path.format(epoch))
            model.save(graph_path.format(epoch))
    
    totaltime = time.time() - before
    print("Time: {} sec  ({} hours)".format(totaltime, totaltime/3600))
           
