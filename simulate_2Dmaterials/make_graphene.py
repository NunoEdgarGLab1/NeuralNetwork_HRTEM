from maker import SampleMakerTrue2D, main
from flake import Flake
from ase import Atoms
import numpy as np

class GrapheneMaker(SampleMakerTrue2D):
    def __init__(self, size, seed):
        a = 2.46
        cell = [[a, 0, 0], [-0.5*a, np.sqrt(3)/2 * a, 0], [0, 0, 2]]
        positions = [[0,0,1], [0, a/np.sqrt(3), 1]]
        prototype = Atoms(symbols='CC', positions=positions, cell=cell, pbc=[True,True,False])
        self.size = size
        self.flake = Flake(prototype, size, seed)

    def make_atoms(self):
        self.flake.make()
        self.flake.rotate()
        self.flake.vacancies(0.1)
        self.flake.perturb_positions(0.1)
        self.flake.tilt(10)
        return self.flake.get_atoms()
    
def makegraphene(first_number, last_number, dir_name, npoints, sampling, seed):
    size = npoints * sampling
    print("Generating images {} - {}".format(first_number, last_number))
    print("Output folder:", dir_name)
    print("Number of point in wave function: {} x {}".format(npoints, npoints))
    print("Sampling: {} Å/pixel".format(sampling))
    print("System size: {:.1f} Å x {:.1f} Å".format(size, size))
    print("Seed:", seed)
    maker = GrapheneMaker((size, size), seed)
    maker.run(first_number, last_number, dir_name, npoints, sampling)


if __name__ == "__main__":
    main(makegraphene, __file__)
    
