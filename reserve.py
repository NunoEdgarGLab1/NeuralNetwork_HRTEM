import sys
sys.path.insert(0, '../')

import os
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.utils import multi_gpu_model
from temnn.knet import Unet
import time
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("lockfile", help="The name of the lock file.")
    args = parser.parse_args()

    lockfile = args.lockfile

    # Determine number of GPUS
    cudavar = 'CUDA_VISIBLE_DEVICES'
    if cudavar in os.environ:
        cudadevices = os.environ[cudavar]
        numgpus = len(cudadevices.split(','))
        print(cudavar, '=', cudadevices)
        print("Found {} GPU devices".format(numgpus))
    else:
        numgpus = 1
        
    image_size = (256, 256)
    image_features = 1
    num_classes = 1
     
    if numgpus > 1:
        with tf.device('/cpu:0'):
            # The master version of the model is locked onto a CPU, to
            # prevent slow GPU-GPU communication and out-of-memory
            # conditions on the hosting GPU.
            x = keras.Input(shape=image_size+(image_features,))
            serial_model = Unet.graph(x, output_features=num_classes)

        model = multi_gpu_model(serial_model, gpus=numgpus)
    else:
        x = keras.Input(shape=image_size+(image_features,))
        model = serial_model = Unet.graph(x, output_features=num_classes)

    # Here we choose the optimizer, it seems to be uncritical.
    model.compile(optimizer='rmsprop', loss='mse',
                  metrics=['accuracy'])

    print()
    print("Waiting for lock file to appear:", lockfile)
    while not os.path.exists(lockfile):
        time.sleep(10)
    print("Lock file appeared - exiting.")
    os.unlink(lockfile)
    
