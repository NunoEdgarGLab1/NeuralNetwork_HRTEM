import sys
sys.path.insert(0, '../')

import os
from datetime import datetime
from glob import glob
import numpy as np
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.utils import multi_gpu_model
from temnn.knet import MSDnet
from temnn.data.dataset import DataSet, DataEntry
from temnn.imagesimul.makeimages import MakeImages
import os
import time
import platform
import shutil
import json
import argparse
import contextlib

## Set constant network parameters
num_epochs = 50 # number of training epochs
save_epochs = 1
#loss_type = 'binary_crossentropy' # mse or binary_cross_entropy
#loss_type = 'mse' # mse or binary_cross_entropy
optimizer = 'rmsprop'
metrics=['accuracy',
        tf.keras.metrics.Precision(),
        tf.keras.metrics.Recall()]

datafolder = '../simulation_data/MoS2_supported_tilt1_smalldisk/'

# Load metadata
with open(os.path.join(datafolder, 'parameters.json')) as json_file:
    parameters = json.load(json_file)
imagedir = os.path.join(datafolder, 'images_labels')

# Load metadata about the validation set
if datafolder[-1] == '/':
    validationdatafolder = datafolder[:-1] + '-test'
else:
    validationdatafolder = datafolder + '-test'
with open(os.path.join(validationdatafolder, 'parameters.json')) as json_file:
    validation_parameters = json.load(json_file)
validation_imagedir = os.path.join(datafolder, 'images_labels')

# Read number of training images
images_per_epoch = parameters['images_per_epoch']
image_epochs = parameters['image_epochs']
parameters['train_epochs'] = num_epochs

# Read number of validation images
validation_images_per_epoch = validation_parameters['images_per_epoch']
validation_image_epochs = validation_parameters['image_epochs']

# Determine number of GPUS
cudavar = 'CUDA_VISIBLE_DEVICES'
if cudavar in os.environ:
    cudadevices = os.environ[cudavar]
    numgpus = len(cudadevices.split(','))
    print(cudavar, '=', cudadevices)
    print("Found {} GPU devices".format(numgpus))
else:
    numgpus = 1
    
# The model is pretty big, we probably can only train on one data
# point at a time without running out of GPU memory.
batch_size = numgpus * 2

image_size = tuple(parameters['image_size']) # spatial dimensions of input/output
if parameters.get('multifocus', None):
    image_features = parameters['multifocus'][0]
else:
    image_features = 1 # depth of input data
num_classes = parameters['num_classes'] # number of predicted class labels
if num_classes > 1:
    loss_type = 'categorical_crossentropy'
else:
    loss_type = 'binary_crossentropy'

num_in_epoch = images_per_epoch//batch_size
num_iterations=num_epochs*num_in_epoch

### Hyperparameter range
layers = np.array([25, 50, 100])
###

## Iterate over layers
for lyrs in layers:
    # Load Network
    if numgpus > 1:
        strategy = tf.distribute.MirroredStrategy()
        strategy_scope = strategy.scope
        print("*** Replicas:", strategy.num_replicas_in_sync)
        print(strategy)
        assert strategy.num_replicas_in_sync == numgpus
    else:
        strategy_scope = contextlib.nullcontext
    with strategy_scope():
        x = keras.Input(shape=image_size+(image_features,))
        model = MSDnet.graph(x, output_features=num_classes,
                            layers=lyrs,
                            reflection_padding=True)
        model.compile(optimizer=optimizer, loss=loss_type,
                      metrics=metrics)

    print("Model summary:", 'MSDnet')
    model.summary()
 
    validation_images = np.empty((validation_images_per_epoch,
                                      image_size[0], image_size[1],
                                      image_features),
                                     np.float32)
    validation_labels = np.empty((validation_images_per_epoch,
                                      image_size[0], image_size[1],
                                      num_classes),
                                     np.float32)
    for i in range(validation_images_per_epoch):
        data = np.load(os.path.join(validation_imagedir,
                                        'image_label_{:03d}_{:04d}.npz'.format(0, i)))
        validation_images[i] = data['image']
        validation_labels[i] = data['label']
        if (i + 1) % 500 == 0:
            print("    loaded {} images.".format(i+1), flush=True)
    
    # Make Output directory
    timestamp = datetime.now().strftime("%Y%m%d-%H%M%S")
    if not os.path.exists('MSDnet_tune_layers'):
        os.makedirs('MSDnet_tune_layers')
    # Logfile for output
    logfile = open(os.path.join('MSDnet_tune_layers',
                    timestamp + '_layers_{}'.format(lyrs) + '.log'),
                    "wt", buffering=1)
    
    # Write headers to the logfile:
    line = ''
    #for m in ['loss',] + metrics:
    for m in ['loss', 'accuracy', 'precision', 'recall']:
        mm = 'val_' + m
        line = line + '{:<15s} {:<15s} '.format(m, mm)
    print(line.strip(), file=logfile, flush=True)
    
    # Optimize the model, saving every save_epoch epoch.
    images = np.empty((images_per_epoch, image_size[0], image_size[1], image_features),
                          np.float32)
    labels = np.empty((images_per_epoch, image_size[0], image_size[1], num_classes),
                          np.float32)
    
    rng = np.random.default_rng()
    print("Starting timing")
    before = time.time()
    for epoch in range(num_epochs):
        actual_epoch = epoch % image_epochs
        summary = None
        batch_no = 1
        print("Loading training epoch {} [{}] / {}.".format(epoch, actual_epoch, num_epochs), flush=True)
        for i in range(images_per_epoch):
            data = np.load(os.path.join(imagedir,
                                            'image_label_{:03d}_{:04d}.npz'.format(actual_epoch, i)))
            images[i] = data['image']
            labels[i] = data['label']
            if (i + 1) % 500 == 0:
                print("    loaded {} images.".format(i+1), flush=True)
        # Random permutation
        permut = rng.permutation(images_per_epoch)
        images = images[permut]
        labels = labels[permut]

        history = model.fit(images, labels, batch_size=batch_size, epochs=1,
                                validation_data=(validation_images, validation_labels))
        history = history.history  # Extract the actual history dictionary
        line = ''
        #for m in ['loss',] + metrics:
        for m in ['loss', 'accuracy', 'precision', 'recall']:
            mm = 'val_' + m
            print("{} = {}    {} = {}".format(m, history[m][0], mm, history[mm][0]), flush=True)
            line = line + '{:<15.8f} {:<15.8f} '.format(history[m][0], history[mm][0])
        print(line.strip(), file=logfile, flush=True)
    
    totaltime = time.time() - before
    print("Time: {} sec  ({} hours)".format(totaltime, totaltime/3600))

