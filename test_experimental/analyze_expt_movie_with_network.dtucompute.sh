#!/bin/bash

#!/bin/bash

if [[ -z "$CONDA_DEFAULT_ENV" ]]; then
    echo "Please activate the right Anaconda envronment before running this script." 1>&2
    exit
fi

if [[ -z "$CUDA_VISIBLE_DEVICES" ]]; then
    echo "Please select the desired GPU by setting CUDA_VISIBLE_DEVICES." 1>&2
    exit
fi

FOLDERNAME=P2_8
REMOTEFOLDER=/work3/jasc/Pei2/$FOLDERNAME
LOCALFOLDER=/scratch/$USER/$FOLDERNAME

mkdir -p $LOCALFOLDER
rsync -av transfer.gbar.dtu.dk:$REMOTEFOLDER/. $LOCALFOLDER/.

python analyze_expt_movie_with_network.py ../Unet_trained_data/Au_cluster-2deg5-Pei2pos $LOCALFOLDER --sampling=0.089

