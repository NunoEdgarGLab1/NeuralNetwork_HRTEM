import sys
sys.path.insert(0, '../')

import matplotlib.pyplot as plt
from glob import glob
import numpy as np
import tensorflow.keras as keras
from tensorflow.keras.utils import multi_gpu_model
import tensorflow as tf
from temnn.knet import net
from temnn.data.dataset import DataEntry,DataSet
from temnn.data.mods import local_normalize
from temnn.loaddata import load_CNN
from pyqstem.imaging import CTF
import matplotlib.pyplot as plt
# Peak detection
from stm.preprocess import normalize
from stm.feature.peaks import find_local_peaks, refine_peaks
from skimage.morphology import disk
from scipy.spatial import cKDTree as KDTree
#import sys
import os
from collections import deque
import hyperspy.api as hs
import skimage.io
import skimage.color

# Location of trained neural network
graph_dir = '../data_from_train/trained_Au_cluster-110-2deg5-big/'
graph_path = os.path.join(graph_dir, 'clusters-4.h5')

# Location of experimental data
data_dir = '../exp_Au_cluster/Pei2/P2_1/Hour_00/Minute_00/Second_00/'
data_file = 'Au_CeO2_P2_1_Hour_00_Minute_00_Second_00_Frame_0002.dm4'

# Load experimental image and verify size
a = hs.load(data_dir+data_file)
image = a.data
image_size = image.shape
print("image size:", image_size, "- Must be divisible by 8.")
# Must be divisible by 8
image_size = tuple(np.array(image_size) // 8 * 8)
print("new image size:", image_size)
image = image[:image_size[0],:image_size[1]]
print("Image size:", image.shape, "- image data type:", image.dtype)

# Load Neural Network 
print("Looking for CNNs in files matching", graph_path)
print("Using CNN parameters in", graph_path)
x, model = load_CNN(graph_path, net.graph, image_size)

# Add Gaussian noise to image
image2 = local_normalize(image, 120, 120)
image2.shape = (1, ) + image_size + (1, )

# Make predictions
predictions = model.predict(image2)

# Locate peaks in the output of the CNN
peaks = find_local_peaks(predictions[0,:,:,0], min_distance=25, 
                         threshold=0.5, exclude_border=10,
                         exclude_adjacent=True)
peaks = refine_peaks(normalize(predictions[0,:,:,0]), peaks, 
                               disk(2), model='polynomial')

# Display results
ig, (ax_raw, ax_pred, ax_atoms) = plt.subplots(3,1,figsize=(15,50))
im1 = ax_raw.imshow(image.T,cmap='gray')
#im2 = ax2.imshow(img2[0,:,:,0].T,cmap='gray')
im2 = ax_pred.imshow(predictions[0,:,:,0].T,cmap='gray')   
im3 = ax_atoms.imshow(image.T, cmap='gray')
ax_atoms.scatter(peaks[:,0], peaks[:,1], c='c', marker='+', linewidth=2.0)
ax_raw.axis('off')
ax_pred.axis('off')
ax_atoms.axis('off')

plt.show()

