"""Create images for training and testing of the neural network."""

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
from pyqstem.imaging import CTF
from temnn.data.labels import create_label
from collections import deque
from multiprocessing import Pool
import os

# Use multiprocessing to generate many sample datasets
class MakeImages:
    def __init__(self, data, parameters, seed=None, batch=None):
        self.data = data
        self.precomputed = []
        if batch is None:
            self.batchsize = 200
        else:
            self.batchsize = batch
        self.parameters = parameters.copy()
        self.imagesize = np.array(parameters['image_size'])
        self.n = 0
        # Check for the max number of processors in the LSB queue system.
        if 'LSB_MAX_NUM_PROCESSORS' in os.environ:
            self.maxcpu = int(os.environ['LSB_MAX_NUM_PROCESSORS'])
            print("Setting max number of CPUs to", self.maxcpu, flush=True)
        else:
            self.maxcpu = None
        # Use a local random number generator that can optionally be seeded.
        self.seed = seed
        self.random = np.random.RandomState(seed)

    def reset(self):
        print("Resetting:", self.data._index_in_epoch)
        self.data._index_in_epoch = 0
        if self.seed:
            self.random.seed(self.seed)

    def makebatch(self, batchsize):
        print("Precomputing {} images.  Debug = {}  n = {}".format(
            batchsize, self.parameters['debug'], self.n), flush=True)
        entries = self.data.next_batch(batchsize, shuffle=self.random)
        sequence = np.arange(self.n, self.n + batchsize)
        self.n += batchsize
        rndnums = self.random.uniform(0.0, 1.0, size=(batchsize, 25))
        params = [self.parameters,] * batchsize
        if self.maxcpu == 1:
            # Don't use multiprocessing to ease debugging.
            return map(makeimage, entries, params, sequence, rndnums)
        with Pool(self.maxcpu) as pool:
            return pool.starmap(makeimage, zip(entries, params, sequence, rndnums))

    def precompute(self):
        self.precomputed = deque(self.makebatch(self.batchsize))
        
    def next_example(self):
        if not self.precomputed:
            self.precompute()
        return self.precomputed.popleft()

    def get_all_examples(self):
        "Get an example from each data point."
        images = []
        labels = []
        for img, lbl in self.makebatch(self.data.num_examples):
            images.append(img)
            labels.append(lbl)
        return np.concatenate(images), np.concatenate(labels)


def show_example(image, label, text, filename):
    channels = image.shape[-1]
    width = 14.0
    fig = plt.figure(figsize=(width,width/(channels+1)))

    for i in range(channels):
        ax = fig.add_subplot(1, channels+1, i+1)

        im = ax.imshow(image[0,:,:,i], interpolation='nearest', cmap='gray')
        divider = make_axes_locatable(ax)
        cax1 = divider.append_axes("right", size="5%", pad=0.05)
        cbar = plt.colorbar(im, cax = cax1)

    ax = fig.add_subplot(1, channels+1, channels+1)
    if label.shape[3] >= 3:
        im = ax.imshow(label[0,:,:,:3].clip(0.0,1.0))
    else:
        im = ax.imshow(label[0,:,:,0], cmap='jet')
    divider = make_axes_locatable(ax)
    cax2 = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(im, cax = cax2)
        
    plt.tight_layout()
    #plt.show()
    fig.savefig(filename+'.png', bbox_inches='tight')
    with open(filename+'.txt', "wt") as info:
        info.write(text)
    plt.close(fig)
    
class randomscale:
    def __init__(self, rnd):
        self.rnd = rnd
        self.n = 0
    def __call__(self, low=0.0, high=1.0):
        r = self.rnd[self.n]
        self.n += 1
        return (high - low) * r + low
    def randint(self, low, high):
        r = self()
        ri = int(np.floor((high - low) * r) + low)
        assert low <= ri < high
        return ri

def makeimage(entry, params, imgnum, rndnums):
    """Make a TEM image.

    entry: A data entry containing at least an exit wave function.

    size:  Size of desired image in pixels (2-tuple).

    imgnum: Sequential number used when plotting in debug mode.

    rndnums: XX random numbers (uniformly in [0;1[).  This prevents
             trouble with random numbers when multiprocessing.
    """
    rnd = randomscale(rndnums)
    size = params['image_size']
    debug = params['debug']
    debug_dir = params['debug_dir']
    
    sampling = rnd(*params['sampling'])
    normalizerange = params['normalizedistance'] / np.mean(params['sampling'])
    spotsize = params['spotsize']  # Size of spots in ground truth - normally 0.4
    Cs = rnd(*params['Cs'])
    defocus = rnd(*params['defocus'])
    if params.get('multifocus') is None:
        multifocus = False
        defocuses = [defocus,]
    else:
        multifocus, deltafocus, deltadeltafocus = params['multifocus']
        defocuses = [defocus,]
        for i in range(1, multifocus):
            defocuses.append(defocuses[-1] + rnd(deltafocus-deltadeltafocus, deltafocus+deltadeltafocus))
        
    focal_spread = rnd(*params['focal_spread'])
    
    aberrations={'a22' : rnd(*params['a22']), 
                'phi22' : rnd(0, 2 * np.pi),
                } #'a40' : 1.4 * 10**6}   # Looks like it is replacing Cs in the code!
    
    dose = 10**rnd(*params['log_dose'])

    mtf_params = [
        rnd(*params['mtf_c0']),
        rnd(*params['mtf_c1']),
        rnd(*params['mtf_c2']),
        rnd(*params['mtf_c3']),
        ]
    
    blur = rnd(*params['blur'])
    
    entry.load()

    for i, defocus in enumerate(defocuses):
        ctf=CTF(defocus=defocus,Cs=Cs,focal_spread=focal_spread,aberrations=aberrations)
    
        entry.create_image(ctf,sampling,blur,dose,mtf_params,concatenate=(i>0))
    
    entry.local_normalize(normalizerange, normalizerange)

    entry.create_label(sampling, width = int(spotsize/sampling),
                       num_classes=params['num_classes'] - 1)
    
    shape = entry._image.shape[1:3]
    assert not ((size[0] > shape[0]) != (size[1] > shape[1]))
    if shape[0] > size[1]:
        assert shape[1] >= size[1]
        entry.random_crop(size, sampling, randint=rnd.randint)
    elif shape[0] < size[1]:
        assert shape[1] <= size[1]
        entry.pad(size)
    else:
        assert shape[1] == size[1]
        
    entry.random_brightness(-.1, .1, rnd=rnd)
    entry.random_contrast(.9, 1.1, rnd=rnd)
    entry.random_gamma(.9, 1.1, rnd=rnd)
    
    entry.random_flip(rnd=rnd)
    image,label=entry.as_tensors()
    entry.reset()

    if debug is True or imgnum < debug:
        text = f"""sampling={sampling} 
Cs={Cs/1.0e4} 
defocus={defocus} 
focal_spread={focal_spread} 
a22={aberrations['a22']} 
dose={dose}
mtf0={mtf_params[0]}
mtf1={mtf_params[1]}
mtf2={mtf_params[2]}
mtf3={mtf_params[3]}
blur={blur}
"""
        fn = os.path.join(debug_dir, "img-{}".format(imgnum))
        show_example(image, label, text, fn)
    
    return image,label

