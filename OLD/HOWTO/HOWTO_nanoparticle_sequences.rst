===================================
HOWTO: Nanoparticle image sequences
===================================

This HOWTO illustrates how to analyze a TEM image sequence of a nanoparticle, looking for e.g. diffusion events.


STEP 1: Create training data
============================

In the folder `simulation` there are scripts for creating the training data.  The script `make_cluster_training_data_110.py` creates training data for a nanoparticle in the <110> zone axis, and the script `make_cluster_testing_data_110.py` creates a corresponding test set.  The scripts identical, a part from the folder where the data is places, and the number of particles generated (1000 for training, 500 for testing).

There are similar scripts for creating nanoparticles in the <100> zone axis.

Ajusting the particle size and misorientation
---------------------------------------------

The scripts currently create nanoparticles with a radius from 5 to 14 layers.  That can be changed by changing line 49::

  radius=5+np.random.rand()*9

If the size is increased, the maximal lattice size of the RandomCluster object should also be increased, this happens in line 43::

  rc=RandomCluster(4.08,14)   # Parameters are lattice constant and grid size

You can also change the misorientation angle of the nanoparticles.  Currently, the nanoparticle may be tilted 2.5 degrees away from the zone axis.  This is determined in line 71::

  alpha=np.random.random()*2.5

For larger particles, this should probably be reduced, as there will otherwise be too many particles where the atomic columns are not visible.

Output of the script
--------------------

The output of the script is placed in a folder, currently named `cluster-110-single-class-2deg5`.  It can be changed in line 29::

  dir_name='../data/cluster-110-single-class-2deg5'

The name should of course reflect the parameters chosen (here <110> zone axis and a tilt of up to 2.5 degrees).

In the folder, exit wave functions are placed in the `wave` sub folder, the `points` subfolder contains information about where the atomic columns are, the `model` subfolder contains the ASE configurations (currenlty not used for training).

The `label` subfolder is currently unused.  In previous versions it contained the ground truth for training, but that is now generated on-the-fly in the training script for flexibility and to save space.

STEP 2: Training a network
==========================

Training is done with the script `ktrain.py`.  You should have `keras` and `tensorflow-gpu` installed in your Python installation.

Setting microscope parameters
-----------------------------

Microscope parameters are randomly picked from an interval for each image.  This interval should include the values that are relevant for the experimental data.  **The most important parameters are the resolution and the defocus.**  Parameters are set in the beginning of the function `makeimage`, in lines 87 - 110.

sampling
  The resolution of the images, in Angstrom per pixel. The
  experimental resolution should be in the middle of the interval, and
  it should go 10-20 % above and below. If you have experimental
  images with widely varying resolution you should train several
  different networks.

normalizerange
  Used for preprocessing of the images, the same value must be used
  for the experimental images.  It is in pixels, and a good value is
  12 Angstrom divided by the resolution (the center of the `sampling`
  interval).

defocus
  When training on nanoparticles, it is important not to mix images
  with positive and negative contrast.  For this reason, defocus
  should either be an interval of positive numbers, or an interval of
  negative numbers.  It is given in Angstrom, 1 micrometer = 1e4 A.

Cs
  Should probably be a symmetric interval around 0, unless you know
  more about the Cs of the microscope.

Running the script
------------------

The script takes a single argument for the name of the output folder.
If you run the script with the argument `mytest`::

  python ktrain.py mytest

then output is places in the folder `graphs-mytest`.  Use different
folders for different networks.

The trained network is places in this folder (A .h5 file for each
epoch, typically you only want to use the last one).  Also, a copy of
the `ktrain.py` script is automatically saved here, so you later can
see what you did.


Checking the training images
----------------------------

The first 50 training images are saved in a subfolder of the `debug`
folder, named for the date and time the script started running. 

Look at the images and check that they are reasonable.  **This can be
checked while the training script is running.**  A microscopist
should be able to tell where most of the atoms are in at least 90-95%
of the images.  If too many images are smeared, it is difficult to
train the network.

STEP 3: Evaluating the trained network
======================================

The script `learningcurve.py` will check performance of the networks
saved by `ktrain.py`.  Note that there is an inconsistency in how the
name of the folder is given, the training script prepends `graphs-` to
the name, but that part must be explicitly specified for this script::

  python ktrain.py mytest
  python learningcurve.py graphs-mytest

Before running the script, please make sure that

* The input folders are correct (`data_dir` should point to the same
  folder that was used in `ktrain.py`, and `validation_dir` to the
  sister-folder with testing data.

* The microscope parameters in lines 37 - 45 are within the intervals
  used for training in `ktrain.py`.
  
The output is a file, `learningcurve.dat`, giving four number for each
network saved in the folder (each epoch).  The four numbers are
precision and recall on images from the training set of nanoparticles,
and precision and recall of images from the test set.

**Precision** is the fraction of the atoms that the network found,
which were correct.  **Recall** is the fraction of the atoms that
should have been found that was actually found.   So recall measures
if the network finds the atoms it should find, precision measures that
it does not find extra atoms.  Both numbers should be close to 1,
preferably above 0.98.  This may not be possible to achieve with all
microscope parameters.

The script `validatedose.py` takes the network from the last epoch,
and test with images with different signal-to-noise ratio.

*Note for machine-learning purists:*  The "training data" used in
these scripts are actually not true training data, as each image is
generated on-the-fly during training.  The "training data set" is
instead *new images* generated from the same nanoparticles as used in
the training, whereas the "test data set" is new images generated from
nanoparticles not used in the training.

STEP 4: Checking the network on experimental data
=================================================

Use the Jupyter notebook `Try expt with CNN.ipynb` to check how the
network performs with your experimental data.


STEP 5: Analyzing an experimental movie
=======================================

The script `analyze_expt_movie.py` does the actual analysis on an
image sequence.  It accepts data in `.dm3` and `.dm4` formats, as well
as most regular image files (for example `.png`).  Often image
sequences are in a folder tree, i.e.
`MyName/Hour_00/Minute_00/Second_12/myname_00_00_12_01.dm4`. The
script supports this kind of structure, and will spit out the analyzed
data in a similar folder tree.

If your input data is aready a movie, you need to split it into
images.  On linux, that is done with the Linux command::

  convert mymovie.mp4 frames%04d.png

This will create image files with each frame, called frames0000.png`, `frames0001-png` etc.

The script  `analyze_expt_movie.py` needs to know the input folder,
the file type, the resolution, and the folder and file name of the
trained neural network.  There is also a `threshold` parameter, it is
the threshold for detecting an atomic column in the output from the
network.  It should normally be around 0.5, but make it smaller if the
network has trouble finding the atoms, and make it larger if it finds
too many spurious atoms.

**The script creates two output folders:**  If your input folder is
`MyMovie`, the output folder `MyMovie_atoms` contains the x-y
positions of the atomic columns (this is the main output folder).

The folder `MyMovie_prediction` contains the raw output from the
network, this is a lot of data and you may not want to keep it.  It
can be useful if you want to change the `theshold` without rerunning
the analysis.

STEP 6A: Using the analysis for making a movie
==============================================

The Jupyter Notebooks `Make Movie XXX.ipynb` illustrate how the output
from the analysis script is used to make a movie where the atomic
columns are shown.

STEP 6B: Using the analysis for studying diffusion
==================================================

Notebooks for doing this will be make available *real soon now*.


